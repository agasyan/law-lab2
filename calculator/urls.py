from django.urls import path
from django.conf.urls import include, url

from calculator import views


#url for app
urlpatterns = [
    #functions
    url(r'^api/calculate/?$', views.calculate, name='calculate'),
    url(r'^$', views.index, name='index'),
]