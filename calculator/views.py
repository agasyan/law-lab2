from django.shortcuts import render

from rest_framework.response import Response

#for function based views
from rest_framework.decorators import api_view

#for class based views
from rest_framework.views import APIView

#for apiroot reverse
from rest_framework.reverse import reverse


def index(request):
    return render(request, 'index.html')

@api_view()
def calculate(request):
    try:
        first_number = float(request.GET.get('a'))
        second_number = float(request.GET.get('b'))
        method = request.GET.get('operation')
        tmphsil = 0
        if method == "Add":
            tmphasil = first_number+second_number
        elif method == "Minus":
            tmphasil = first_number-second_number
        elif method == "Division":
            tmphasil = first_number/second_number
        elif method == "Multiply":
            tmphasil = first_number*second_number
        else:
            return Response({'function': method,'status': 'not recognized'})
        return Response({'function': method,'result': tmphasil, 'status': 'success'})
    except Exception as e:
        return Response({'function': method,'result': 'there was an error ' + str(e), 'status': 'error'})
